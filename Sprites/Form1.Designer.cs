﻿namespace Sprites
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.нарисоватьЛиниюToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.введитеXToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.введитеХToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripTextBoxX = new System.Windows.Forms.ToolStripTextBox();
            this.введитеYToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripTextBoxY = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.введитеX2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripTextBoxX2 = new System.Windows.Forms.ToolStripTextBox();
            this.введитеУ2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripTextBoxY2 = new System.Windows.Forms.ToolStripTextBox();
            this.подтвердитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.нарисоватьЭлипсToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.кругToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.введитеDToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripTextBoxDiametr = new System.Windows.Forms.ToolStripTextBox();
            this.введитеПоложениеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripTextBoxXCercal = new System.Windows.Forms.ToolStripTextBox();
            this.введитеПYToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripTextBoxYCercal = new System.Windows.Forms.ToolStripTextBox();
            this.подтвердитьToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.овалToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.высотаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripTextBoxOvalH = new System.Windows.Forms.ToolStripTextBox();
            this.ширинаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripTextBoxOvalW = new System.Windows.Forms.ToolStripTextBox();
            this.введитеПХToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripTextBoxOvalX = new System.Windows.Forms.ToolStripTextBox();
            this.введитеПУToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripTextBoxOvalY = new System.Windows.Forms.ToolStripTextBox();
            this.подтвердитьToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.нарисоватьПрямоугольникToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.вводДанныхToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.введитеВысотуToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripTextBoxRectH = new System.Windows.Forms.ToolStripTextBox();
            this.введитеШиринуToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripTextBoxRectW = new System.Windows.Forms.ToolStripTextBox();
            this.введитеПХToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripTextBoxRectX = new System.Windows.Forms.ToolStripTextBox();
            this.введитеПУToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripTextBoxRectY = new System.Windows.Forms.ToolStripTextBox();
            this.подтвердитьToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.нарисоватьТреугольникToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.очиститьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.вводДанныхToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.введитеОснованиеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripTextBoxTA = new System.Windows.Forms.ToolStripTextBox();
            this.введитеПbToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripTextBoxTB = new System.Windows.Forms.ToolStripTextBox();
            this.введитеПсToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripTextBoxTC = new System.Windows.Forms.ToolStripTextBox();
            this.введитеХToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripTextBoxTX = new System.Windows.Forms.ToolStripTextBox();
            this.введитеУToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripTextBoxTY = new System.Windows.Forms.ToolStripTextBox();
            this.подтвердитьToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox
            // 
            this.pictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox.Location = new System.Drawing.Point(0, 28);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(893, 520);
            this.pictureBox.TabIndex = 0;
            this.pictureBox.TabStop = false;
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.нарисоватьЛиниюToolStripMenuItem,
            this.нарисоватьЭлипсToolStripMenuItem,
            this.нарисоватьПрямоугольникToolStripMenuItem,
            this.нарисоватьТреугольникToolStripMenuItem,
            this.очиститьToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(893, 28);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // нарисоватьЛиниюToolStripMenuItem
            // 
            this.нарисоватьЛиниюToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.введитеXToolStripMenuItem});
            this.нарисоватьЛиниюToolStripMenuItem.Name = "нарисоватьЛиниюToolStripMenuItem";
            this.нарисоватьЛиниюToolStripMenuItem.Size = new System.Drawing.Size(161, 24);
            this.нарисоватьЛиниюToolStripMenuItem.Text = "Нарисовать линию-";
            // 
            // введитеXToolStripMenuItem
            // 
            this.введитеXToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.введитеХToolStripMenuItem,
            this.toolStripTextBoxX,
            this.введитеYToolStripMenuItem,
            this.toolStripTextBoxY,
            this.toolStripSeparator1,
            this.введитеX2ToolStripMenuItem,
            this.toolStripTextBoxX2,
            this.введитеУ2ToolStripMenuItem,
            this.toolStripTextBoxY2,
            this.подтвердитьToolStripMenuItem});
            this.введитеXToolStripMenuItem.Name = "введитеXToolStripMenuItem";
            this.введитеXToolStripMenuItem.Size = new System.Drawing.Size(181, 26);
            this.введитеXToolStripMenuItem.Text = "Ввод данных";
            // 
            // введитеХToolStripMenuItem
            // 
            this.введитеХToolStripMenuItem.Enabled = false;
            this.введитеХToolStripMenuItem.Name = "введитеХToolStripMenuItem";
            this.введитеХToolStripMenuItem.Size = new System.Drawing.Size(181, 26);
            this.введитеХToolStripMenuItem.Text = "Введите х";
            // 
            // toolStripTextBoxX
            // 
            this.toolStripTextBoxX.Name = "toolStripTextBoxX";
            this.toolStripTextBoxX.Size = new System.Drawing.Size(100, 27);
            // 
            // введитеYToolStripMenuItem
            // 
            this.введитеYToolStripMenuItem.Enabled = false;
            this.введитеYToolStripMenuItem.Name = "введитеYToolStripMenuItem";
            this.введитеYToolStripMenuItem.Size = new System.Drawing.Size(174, 26);
            this.введитеYToolStripMenuItem.Text = "Введите y";
            // 
            // toolStripTextBoxY
            // 
            this.toolStripTextBoxY.Name = "toolStripTextBoxY";
            this.toolStripTextBoxY.Size = new System.Drawing.Size(100, 27);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(171, 6);
            // 
            // введитеX2ToolStripMenuItem
            // 
            this.введитеX2ToolStripMenuItem.Enabled = false;
            this.введитеX2ToolStripMenuItem.Name = "введитеX2ToolStripMenuItem";
            this.введитеX2ToolStripMenuItem.Size = new System.Drawing.Size(174, 26);
            this.введитеX2ToolStripMenuItem.Text = "Введите x2";
            // 
            // toolStripTextBoxX2
            // 
            this.toolStripTextBoxX2.Name = "toolStripTextBoxX2";
            this.toolStripTextBoxX2.Size = new System.Drawing.Size(100, 27);
            // 
            // введитеУ2ToolStripMenuItem
            // 
            this.введитеУ2ToolStripMenuItem.Enabled = false;
            this.введитеУ2ToolStripMenuItem.Name = "введитеУ2ToolStripMenuItem";
            this.введитеУ2ToolStripMenuItem.Size = new System.Drawing.Size(174, 26);
            this.введитеУ2ToolStripMenuItem.Text = "Введите у2";
            // 
            // toolStripTextBoxY2
            // 
            this.toolStripTextBoxY2.Name = "toolStripTextBoxY2";
            this.toolStripTextBoxY2.Size = new System.Drawing.Size(100, 27);
            // 
            // подтвердитьToolStripMenuItem
            // 
            this.подтвердитьToolStripMenuItem.Name = "подтвердитьToolStripMenuItem";
            this.подтвердитьToolStripMenuItem.Size = new System.Drawing.Size(174, 26);
            this.подтвердитьToolStripMenuItem.Text = "Подтвердить";
            this.подтвердитьToolStripMenuItem.Click += new System.EventHandler(this.подтвердитьToolStripMenuItem_Click);
            // 
            // нарисоватьЭлипсToolStripMenuItem
            // 
            this.нарисоватьЭлипсToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.кругToolStripMenuItem,
            this.овалToolStripMenuItem});
            this.нарисоватьЭлипсToolStripMenuItem.Name = "нарисоватьЭлипсToolStripMenuItem";
            this.нарисоватьЭлипсToolStripMenuItem.Size = new System.Drawing.Size(148, 24);
            this.нарисоватьЭлипсToolStripMenuItem.Text = "Нарисовать элипс";
            // 
            // кругToolStripMenuItem
            // 
            this.кругToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.введитеDToolStripMenuItem,
            this.toolStripTextBoxDiametr,
            this.введитеПоложениеToolStripMenuItem,
            this.toolStripTextBoxXCercal,
            this.введитеПYToolStripMenuItem,
            this.toolStripTextBoxYCercal,
            this.подтвердитьToolStripMenuItem1});
            this.кругToolStripMenuItem.Name = "кругToolStripMenuItem";
            this.кругToolStripMenuItem.Size = new System.Drawing.Size(181, 26);
            this.кругToolStripMenuItem.Text = "Круг";
            // 
            // введитеDToolStripMenuItem
            // 
            this.введитеDToolStripMenuItem.Enabled = false;
            this.введитеDToolStripMenuItem.Name = "введитеDToolStripMenuItem";
            this.введитеDToolStripMenuItem.Size = new System.Drawing.Size(181, 26);
            this.введитеDToolStripMenuItem.Text = "Введите D";
            // 
            // toolStripTextBoxDiametr
            // 
            this.toolStripTextBoxDiametr.Name = "toolStripTextBoxDiametr";
            this.toolStripTextBoxDiametr.Size = new System.Drawing.Size(100, 27);
            // 
            // введитеПоложениеToolStripMenuItem
            // 
            this.введитеПоложениеToolStripMenuItem.Enabled = false;
            this.введитеПоложениеToolStripMenuItem.Name = "введитеПоложениеToolStripMenuItem";
            this.введитеПоложениеToolStripMenuItem.Size = new System.Drawing.Size(181, 26);
            this.введитеПоложениеToolStripMenuItem.Text = "Введите п.X";
            // 
            // toolStripTextBoxXCercal
            // 
            this.toolStripTextBoxXCercal.Name = "toolStripTextBoxXCercal";
            this.toolStripTextBoxXCercal.Size = new System.Drawing.Size(100, 27);
            // 
            // введитеПYToolStripMenuItem
            // 
            this.введитеПYToolStripMenuItem.Enabled = false;
            this.введитеПYToolStripMenuItem.Name = "введитеПYToolStripMenuItem";
            this.введитеПYToolStripMenuItem.Size = new System.Drawing.Size(181, 26);
            this.введитеПYToolStripMenuItem.Text = "Введите п.Y";
            // 
            // toolStripTextBoxYCercal
            // 
            this.toolStripTextBoxYCercal.Name = "toolStripTextBoxYCercal";
            this.toolStripTextBoxYCercal.Size = new System.Drawing.Size(100, 27);
            // 
            // подтвердитьToolStripMenuItem1
            // 
            this.подтвердитьToolStripMenuItem1.Name = "подтвердитьToolStripMenuItem1";
            this.подтвердитьToolStripMenuItem1.Size = new System.Drawing.Size(174, 26);
            this.подтвердитьToolStripMenuItem1.Text = "Подтвердить";
            this.подтвердитьToolStripMenuItem1.Click += new System.EventHandler(this.подтвердитьToolStripMenuItem1_Click);
            // 
            // овалToolStripMenuItem
            // 
            this.овалToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.высотаToolStripMenuItem,
            this.toolStripTextBoxOvalH,
            this.ширинаToolStripMenuItem,
            this.toolStripTextBoxOvalW,
            this.введитеПХToolStripMenuItem,
            this.toolStripTextBoxOvalX,
            this.введитеПУToolStripMenuItem,
            this.toolStripTextBoxOvalY,
            this.подтвердитьToolStripMenuItem2});
            this.овалToolStripMenuItem.Name = "овалToolStripMenuItem";
            this.овалToolStripMenuItem.Size = new System.Drawing.Size(181, 26);
            this.овалToolStripMenuItem.Text = "Овал";
            // 
            // высотаToolStripMenuItem
            // 
            this.высотаToolStripMenuItem.Enabled = false;
            this.высотаToolStripMenuItem.Name = "высотаToolStripMenuItem";
            this.высотаToolStripMenuItem.Size = new System.Drawing.Size(181, 26);
            this.высотаToolStripMenuItem.Text = "Высота";
            // 
            // toolStripTextBoxOvalH
            // 
            this.toolStripTextBoxOvalH.Name = "toolStripTextBoxOvalH";
            this.toolStripTextBoxOvalH.Size = new System.Drawing.Size(100, 27);
            // 
            // ширинаToolStripMenuItem
            // 
            this.ширинаToolStripMenuItem.Enabled = false;
            this.ширинаToolStripMenuItem.Name = "ширинаToolStripMenuItem";
            this.ширинаToolStripMenuItem.Size = new System.Drawing.Size(181, 26);
            this.ширинаToolStripMenuItem.Text = "Ширина";
            // 
            // toolStripTextBoxOvalW
            // 
            this.toolStripTextBoxOvalW.Name = "toolStripTextBoxOvalW";
            this.toolStripTextBoxOvalW.Size = new System.Drawing.Size(100, 27);
            // 
            // введитеПХToolStripMenuItem
            // 
            this.введитеПХToolStripMenuItem.Enabled = false;
            this.введитеПХToolStripMenuItem.Name = "введитеПХToolStripMenuItem";
            this.введитеПХToolStripMenuItem.Size = new System.Drawing.Size(181, 26);
            this.введитеПХToolStripMenuItem.Text = "Введите п.Х";
            // 
            // toolStripTextBoxOvalX
            // 
            this.toolStripTextBoxOvalX.Name = "toolStripTextBoxOvalX";
            this.toolStripTextBoxOvalX.Size = new System.Drawing.Size(100, 27);
            // 
            // введитеПУToolStripMenuItem
            // 
            this.введитеПУToolStripMenuItem.Enabled = false;
            this.введитеПУToolStripMenuItem.Name = "введитеПУToolStripMenuItem";
            this.введитеПУToolStripMenuItem.Size = new System.Drawing.Size(181, 26);
            this.введитеПУToolStripMenuItem.Text = "Введите п.У";
            // 
            // toolStripTextBoxOvalY
            // 
            this.toolStripTextBoxOvalY.Name = "toolStripTextBoxOvalY";
            this.toolStripTextBoxOvalY.Size = new System.Drawing.Size(100, 27);
            // 
            // подтвердитьToolStripMenuItem2
            // 
            this.подтвердитьToolStripMenuItem2.Name = "подтвердитьToolStripMenuItem2";
            this.подтвердитьToolStripMenuItem2.Size = new System.Drawing.Size(174, 26);
            this.подтвердитьToolStripMenuItem2.Text = "Подтвердить";
            this.подтвердитьToolStripMenuItem2.Click += new System.EventHandler(this.подтвердитьToolStripMenuItem2_Click);
            // 
            // нарисоватьПрямоугольникToolStripMenuItem
            // 
            this.нарисоватьПрямоугольникToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.вводДанныхToolStripMenuItem});
            this.нарисоватьПрямоугольникToolStripMenuItem.Name = "нарисоватьПрямоугольникToolStripMenuItem";
            this.нарисоватьПрямоугольникToolStripMenuItem.Size = new System.Drawing.Size(217, 24);
            this.нарисоватьПрямоугольникToolStripMenuItem.Text = "Нарисовать прямоугольник";
            // 
            // вводДанныхToolStripMenuItem
            // 
            this.вводДанныхToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.введитеВысотуToolStripMenuItem,
            this.toolStripTextBoxRectH,
            this.введитеШиринуToolStripMenuItem,
            this.toolStripTextBoxRectW,
            this.введитеПХToolStripMenuItem1,
            this.toolStripTextBoxRectX,
            this.введитеПУToolStripMenuItem1,
            this.toolStripTextBoxRectY,
            this.подтвердитьToolStripMenuItem3});
            this.вводДанныхToolStripMenuItem.Name = "вводДанныхToolStripMenuItem";
            this.вводДанныхToolStripMenuItem.Size = new System.Drawing.Size(181, 26);
            this.вводДанныхToolStripMenuItem.Text = "Ввод данных";
            // 
            // введитеВысотуToolStripMenuItem
            // 
            this.введитеВысотуToolStripMenuItem.Enabled = false;
            this.введитеВысотуToolStripMenuItem.Name = "введитеВысотуToolStripMenuItem";
            this.введитеВысотуToolStripMenuItem.Size = new System.Drawing.Size(199, 26);
            this.введитеВысотуToolStripMenuItem.Text = "Введите высоту";
            // 
            // toolStripTextBoxRectH
            // 
            this.toolStripTextBoxRectH.Name = "toolStripTextBoxRectH";
            this.toolStripTextBoxRectH.Size = new System.Drawing.Size(100, 27);
            // 
            // введитеШиринуToolStripMenuItem
            // 
            this.введитеШиринуToolStripMenuItem.Enabled = false;
            this.введитеШиринуToolStripMenuItem.Name = "введитеШиринуToolStripMenuItem";
            this.введитеШиринуToolStripMenuItem.Size = new System.Drawing.Size(199, 26);
            this.введитеШиринуToolStripMenuItem.Text = "Введите ширину";
            // 
            // toolStripTextBoxRectW
            // 
            this.toolStripTextBoxRectW.Name = "toolStripTextBoxRectW";
            this.toolStripTextBoxRectW.Size = new System.Drawing.Size(100, 27);
            // 
            // введитеПХToolStripMenuItem1
            // 
            this.введитеПХToolStripMenuItem1.Enabled = false;
            this.введитеПХToolStripMenuItem1.Name = "введитеПХToolStripMenuItem1";
            this.введитеПХToolStripMenuItem1.Size = new System.Drawing.Size(199, 26);
            this.введитеПХToolStripMenuItem1.Text = "Введите п. Х";
            // 
            // toolStripTextBoxRectX
            // 
            this.toolStripTextBoxRectX.Name = "toolStripTextBoxRectX";
            this.toolStripTextBoxRectX.Size = new System.Drawing.Size(100, 27);
            // 
            // введитеПУToolStripMenuItem1
            // 
            this.введитеПУToolStripMenuItem1.Enabled = false;
            this.введитеПУToolStripMenuItem1.Name = "введитеПУToolStripMenuItem1";
            this.введитеПУToolStripMenuItem1.Size = new System.Drawing.Size(199, 26);
            this.введитеПУToolStripMenuItem1.Text = "Введите п.У";
            // 
            // toolStripTextBoxRectY
            // 
            this.toolStripTextBoxRectY.Name = "toolStripTextBoxRectY";
            this.toolStripTextBoxRectY.Size = new System.Drawing.Size(100, 27);
            // 
            // подтвердитьToolStripMenuItem3
            // 
            this.подтвердитьToolStripMenuItem3.Name = "подтвердитьToolStripMenuItem3";
            this.подтвердитьToolStripMenuItem3.Size = new System.Drawing.Size(199, 26);
            this.подтвердитьToolStripMenuItem3.Text = "Подтвердить";
            this.подтвердитьToolStripMenuItem3.Click += new System.EventHandler(this.подтвердитьToolStripMenuItem3_Click);
            // 
            // нарисоватьТреугольникToolStripMenuItem
            // 
            this.нарисоватьТреугольникToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.вводДанныхToolStripMenuItem1});
            this.нарисоватьТреугольникToolStripMenuItem.Name = "нарисоватьТреугольникToolStripMenuItem";
            this.нарисоватьТреугольникToolStripMenuItem.Size = new System.Drawing.Size(194, 24);
            this.нарисоватьТреугольникToolStripMenuItem.Text = "Нарисовать треугольник";
            // 
            // очиститьToolStripMenuItem
            // 
            this.очиститьToolStripMenuItem.Name = "очиститьToolStripMenuItem";
            this.очиститьToolStripMenuItem.Size = new System.Drawing.Size(85, 24);
            this.очиститьToolStripMenuItem.Text = "Очистить";
            this.очиститьToolStripMenuItem.Click += new System.EventHandler(this.очиститьToolStripMenuItem_Click);
            // 
            // вводДанныхToolStripMenuItem1
            // 
            this.вводДанныхToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.введитеОснованиеToolStripMenuItem,
            this.toolStripTextBoxTA,
            this.введитеПbToolStripMenuItem,
            this.toolStripTextBoxTB,
            this.введитеПсToolStripMenuItem,
            this.toolStripTextBoxTC,
            this.введитеХToolStripMenuItem1,
            this.toolStripTextBoxTX,
            this.введитеУToolStripMenuItem,
            this.toolStripTextBoxTY,
            this.подтвердитьToolStripMenuItem4});
            this.вводДанныхToolStripMenuItem1.Name = "вводДанныхToolStripMenuItem1";
            this.вводДанныхToolStripMenuItem1.Size = new System.Drawing.Size(181, 26);
            this.вводДанныхToolStripMenuItem1.Text = "Ввод данных";
            // 
            // введитеОснованиеToolStripMenuItem
            // 
            this.введитеОснованиеToolStripMenuItem.Enabled = false;
            this.введитеОснованиеToolStripMenuItem.Name = "введитеОснованиеToolStripMenuItem";
            this.введитеОснованиеToolStripMenuItem.Size = new System.Drawing.Size(181, 26);
            this.введитеОснованиеToolStripMenuItem.Text = "Введите п.а";
            // 
            // toolStripTextBoxTA
            // 
            this.toolStripTextBoxTA.Name = "toolStripTextBoxTA";
            this.toolStripTextBoxTA.Size = new System.Drawing.Size(100, 27);
            // 
            // введитеПbToolStripMenuItem
            // 
            this.введитеПbToolStripMenuItem.Enabled = false;
            this.введитеПbToolStripMenuItem.Name = "введитеПbToolStripMenuItem";
            this.введитеПbToolStripMenuItem.Size = new System.Drawing.Size(181, 26);
            this.введитеПbToolStripMenuItem.Text = "Введите п.b";
            // 
            // toolStripTextBoxTB
            // 
            this.toolStripTextBoxTB.Name = "toolStripTextBoxTB";
            this.toolStripTextBoxTB.Size = new System.Drawing.Size(100, 27);
            // 
            // введитеПсToolStripMenuItem
            // 
            this.введитеПсToolStripMenuItem.Enabled = false;
            this.введитеПсToolStripMenuItem.Name = "введитеПсToolStripMenuItem";
            this.введитеПсToolStripMenuItem.Size = new System.Drawing.Size(181, 26);
            this.введитеПсToolStripMenuItem.Text = "Введите п.с";
            // 
            // toolStripTextBoxTC
            // 
            this.toolStripTextBoxTC.Name = "toolStripTextBoxTC";
            this.toolStripTextBoxTC.Size = new System.Drawing.Size(100, 27);
            // 
            // введитеХToolStripMenuItem1
            // 
            this.введитеХToolStripMenuItem1.Enabled = false;
            this.введитеХToolStripMenuItem1.Name = "введитеХToolStripMenuItem1";
            this.введитеХToolStripMenuItem1.Size = new System.Drawing.Size(181, 26);
            this.введитеХToolStripMenuItem1.Text = "Введите Х";
            // 
            // toolStripTextBoxTX
            // 
            this.toolStripTextBoxTX.Name = "toolStripTextBoxTX";
            this.toolStripTextBoxTX.Size = new System.Drawing.Size(100, 27);
            // 
            // введитеУToolStripMenuItem
            // 
            this.введитеУToolStripMenuItem.Enabled = false;
            this.введитеУToolStripMenuItem.Name = "введитеУToolStripMenuItem";
            this.введитеУToolStripMenuItem.Size = new System.Drawing.Size(181, 26);
            this.введитеУToolStripMenuItem.Text = "Введите У";
            // 
            // toolStripTextBoxTY
            // 
            this.toolStripTextBoxTY.Name = "toolStripTextBoxTY";
            this.toolStripTextBoxTY.Size = new System.Drawing.Size(100, 27);
            // 
            // подтвердитьToolStripMenuItem4
            // 
            this.подтвердитьToolStripMenuItem4.Name = "подтвердитьToolStripMenuItem4";
            this.подтвердитьToolStripMenuItem4.Size = new System.Drawing.Size(181, 26);
            this.подтвердитьToolStripMenuItem4.Text = "Подтвердить";
            this.подтвердитьToolStripMenuItem4.Click += new System.EventHandler(this.подтвердитьToolStripMenuItem4_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(893, 548);
            this.Controls.Add(this.pictureBox);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem нарисоватьЛиниюToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem введитеXToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem введитеХToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBoxX;
        private System.Windows.Forms.ToolStripMenuItem введитеYToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBoxY;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem введитеX2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBoxX2;
        private System.Windows.Forms.ToolStripMenuItem введитеУ2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBoxY2;
        private System.Windows.Forms.ToolStripMenuItem подтвердитьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem нарисоватьЭлипсToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem кругToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem введитеDToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBoxDiametr;
        private System.Windows.Forms.ToolStripMenuItem введитеПоложениеToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBoxXCercal;
        private System.Windows.Forms.ToolStripMenuItem введитеПYToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBoxYCercal;
        private System.Windows.Forms.ToolStripMenuItem подтвердитьToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem овалToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem высотаToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBoxOvalH;
        private System.Windows.Forms.ToolStripMenuItem ширинаToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBoxOvalW;
        private System.Windows.Forms.ToolStripMenuItem введитеПХToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBoxOvalX;
        private System.Windows.Forms.ToolStripMenuItem введитеПУToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBoxOvalY;
        private System.Windows.Forms.ToolStripMenuItem подтвердитьToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem нарисоватьПрямоугольникToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem вводДанныхToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem введитеВысотуToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBoxRectH;
        private System.Windows.Forms.ToolStripMenuItem введитеШиринуToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBoxRectW;
        private System.Windows.Forms.ToolStripMenuItem введитеПХToolStripMenuItem1;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBoxRectX;
        private System.Windows.Forms.ToolStripMenuItem введитеПУToolStripMenuItem1;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBoxRectY;
        private System.Windows.Forms.ToolStripMenuItem подтвердитьToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem нарисоватьТреугольникToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem очиститьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem вводДанныхToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem введитеОснованиеToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBoxTA;
        private System.Windows.Forms.ToolStripMenuItem введитеПbToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBoxTB;
        private System.Windows.Forms.ToolStripMenuItem введитеПсToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBoxTC;
        private System.Windows.Forms.ToolStripMenuItem введитеХToolStripMenuItem1;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBoxTX;
        private System.Windows.Forms.ToolStripMenuItem введитеУToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBoxTY;
        private System.Windows.Forms.ToolStripMenuItem подтвердитьToolStripMenuItem4;
    }
}

