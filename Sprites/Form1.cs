﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sprites
{
    public partial class Form1 : Form
    {
        private Brush red = new SolidBrush(Color.Red);
        private Pen redPen = new Pen(Color.Red, 5);
        public Form1()
        {
            InitializeComponent();
            this.BackColor = Color.Black;
            menuStrip1.ForeColor = Color.Green;
            menuStrip1.BackColor = Color.Black;
        }

        private void подтвердитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Graphics graphics = pictureBox.CreateGraphics();

            int x, y, x2, y2;

            x = int.Parse(toolStripTextBoxX.Text);
            x2 = int.Parse(toolStripTextBoxX2.Text);


            y = int.Parse(toolStripTextBoxY.Text);
            y2 = int.Parse(toolStripTextBoxY2.Text);

            graphics.DrawLine(redPen, x, y, x2, y2);
        }

        private void удалитьЛиниюToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pictureBox.Refresh();
        }

        private void подтвердитьToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Graphics graphics = pictureBox.CreateGraphics();

            int d, x, y;

            x = int.Parse(toolStripTextBoxXCercal.Text);
            y = int.Parse(toolStripTextBoxYCercal.Text);
            d = int.Parse(toolStripTextBoxDiametr.Text);

            graphics.DrawEllipse(redPen, x, y, d / 2, d / 2);
        }

        private void подтвердитьToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            Graphics graphics = pictureBox.CreateGraphics();

            int heght, widht, x, y;

            x = int.Parse(toolStripTextBoxOvalX.Text);
            y = int.Parse(toolStripTextBoxOvalY.Text);
            widht = int.Parse(toolStripTextBoxOvalW.Text);
            heght = int.Parse(toolStripTextBoxOvalH.Text);

            graphics.DrawEllipse(redPen, x, y, widht, heght);
        }

        private void подтвердитьToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            Graphics graphics = pictureBox.CreateGraphics();


            int heght, widht, x, y;

            x = int.Parse(toolStripTextBoxRectX.Text);
            y = int.Parse(toolStripTextBoxRectY.Text);
            widht = int.Parse(toolStripTextBoxRectW.Text);
            heght = int.Parse(toolStripTextBoxRectH.Text);

            graphics.DrawRectangle(redPen, x, y, widht, heght);
        }

        private void очиститьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pictureBox.Refresh();
        }

        private void подтвердитьToolStripMenuItem4_Click(object sender, EventArgs e)
        {
            Graphics graphics = pictureBox.CreateGraphics();

            int a, b, c, x, y;

            a = int.Parse(toolStripTextBoxTA.Text);
            b = int.Parse(toolStripTextBoxTB.Text);
            c = int.Parse(toolStripTextBoxTC.Text);
            x = int.Parse(toolStripTextBoxTX.Text);
            y = int.Parse(toolStripTextBoxTY.Text);

            Point[] points = new Point[3];

            points[0] = new Point(y,a);
            points[1] = new Point(x, b);
            points[2] = new Point(c, b);

            graphics.DrawPolygon(redPen,points);
        }
    }
}

